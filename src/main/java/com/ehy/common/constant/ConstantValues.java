package com.ehy.common.constant;

import com.ehy.common.helper.CommonHelper;

public class ConstantValues {

  public static final String CURRENCY_IDR = "IDR";
  public static final String CUSTOMER_IP_ADDRESS_INTERNAL = "INTERNAL";
  public static final String CUSTOMER_SESSION_ID_INTERNAL = "INTERNAL";
  public static final String REQUEST_ID = CommonHelper.generateUUID();
  public static final String SYSTEM = "SYSTEM";
  public static final String NO_REPLY_EMAIL = "noreply@ehy.com";
}
