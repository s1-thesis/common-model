package com.ehy.common.constant;

public class KafkaTopic {

  public static final String PREFIX = "com.ehy.";
  public static final String SEND_EMAIL = PREFIX + ServiceId.MESSAGE_SERVICE + ".email";
}
