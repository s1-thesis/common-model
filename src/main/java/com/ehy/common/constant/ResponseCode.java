package com.ehy.common.constant;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum ResponseCode {

  SUCCESS("SUCCESS", "SUCCESS"),
  SYSTEM_ERROR("SYSTEM_ERROR", "Contact our team"),
  DUPLICATE_DATA("DUPLICATE_DATA", "Duplicate data"),
  DATA_NOT_EXIST("DATA_NOT_EXIST", "No data exist"),
  BIND_ERROR("BIND_ERROR", "Please fill in mandatory parameter"),
  RUNTIME_ERROR("RUNTIME_ERROR", "Runtime Error"),
  INVALID_JSON_FORMAT("INVALID_JSON_FORMAT", "Invalid JSON format"),
  THIRD_PARTY_ERROR("THIRD_PARTY_ERROR", "Third party error"),
  DATE_NOT_VALID("DATE_NOT_VALID", "Date not valid");

  private String code;
  private String message;

  ResponseCode(String code, String message) {
    this.code = code;
    this.message = message;
  }
}
