package com.ehy.common.constant;

public class ServiceId {

  public static final String PAYMENT_SERVICE = "payment-service";
  public static final String USER_SERVICE = "user-service";
  public static final String ORDER_SERVICE = "order-service";
  public static final String PRODUCT_SERVICE = "product-service";
  public static final String MESSAGE_SERVICE = "message-service";
}
