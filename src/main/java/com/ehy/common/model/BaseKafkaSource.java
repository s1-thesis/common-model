package com.ehy.common.model;

import com.ehy.common.model.kafka.DocumentKey;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseKafkaSource<T> {

  private String operationType;
  private T fullDocument;
  private DocumentKey documentKey;
}
