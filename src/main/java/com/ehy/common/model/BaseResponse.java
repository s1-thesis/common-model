package com.ehy.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.time.Instant;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseResponse<T> {

  private String code;
  private String message;
  private List<String> errors;
  private T data;
  private Instant unixTimestamp;

  public BaseResponse() {
  }

  @Builder(builderMethodName = "parentBuilder")
  public BaseResponse(String code, String message, List<String> errors, T data,
      Instant unixTimestamp) {
    this.code = code;
    this.message = message;
    this.errors = errors;
    this.data = data;
    this.unixTimestamp = unixTimestamp;
  }
}
