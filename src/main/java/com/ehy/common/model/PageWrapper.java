package com.ehy.common.model;

import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PageWrapper<T> {

  private PageInfo pageInfo;
  private List<T> content;

  @Builder
  public PageWrapper(PageInfo pageInfo, List<T> content) {
    this.pageInfo = pageInfo;
    this.content = content;
  }
}
