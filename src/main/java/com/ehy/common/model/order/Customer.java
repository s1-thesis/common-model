package com.ehy.common.model.order;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@Builder
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {

  private String firstName;
  private String lastName;
  private String email;
  private String phone;
  private String address;
  private String city;
  private String postalCode;
  private String countryCode;
}
