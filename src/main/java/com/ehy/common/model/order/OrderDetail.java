package com.ehy.common.model.order;

import com.ehy.common.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDetail extends BaseDto<String> {

  private List<InventoryDetail> inventoryDetails;
  private String userId;
  private List<Customer> customers;

  @Builder
  public OrderDetail(String id,
      List<InventoryDetail> inventoryDetails, String userId,
      List<Customer> customers) {
    super(id);
    this.inventoryDetails = inventoryDetails;
    this.userId = userId;
    this.customers = customers;
  }
}
