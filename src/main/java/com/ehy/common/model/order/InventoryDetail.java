package com.ehy.common.model.order;

import com.ehy.common.model.BaseDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class InventoryDetail extends BaseDto<String> {

  private Double price;
  private Integer quantity;
  private String airlineCode;
  private String date;
  private String currency = "IDR";

  @Builder
  public InventoryDetail(String id, Double price, Integer quantity, String airlineCode,
      String date, String currency) {
    super(id);
    this.price = price;
    this.quantity = quantity;
    this.airlineCode = airlineCode;
    this.date = date;
    this.currency = currency;
  }
}
