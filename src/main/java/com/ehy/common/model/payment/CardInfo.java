package com.ehy.common.model.payment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CardInfo {

  @Length(min = 16, max = 16)
  private String cardNumber;

  @Length(min = 3, max = 3)
  private String cvv;

  @Length(min = 2, max = 2)
  private String expireMonth;

  @Length(min = 2, max = 2)
  private String expireYear;
}
