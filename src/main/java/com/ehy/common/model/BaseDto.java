package com.ehy.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import org.bson.codecs.pojo.annotations.BsonProperty;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseDto<T> {

  @JsonProperty("id")
  @BsonProperty("id")
  private T id;

  public BaseDto() {
  }

  public BaseDto(T id) {
    this.id = id;
  }
}
