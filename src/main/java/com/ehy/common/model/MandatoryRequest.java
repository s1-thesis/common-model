package com.ehy.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotBlank;
import javax.ws.rs.DefaultValue;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jboss.resteasy.annotations.jaxrs.HeaderParam;

@Data
@NoArgsConstructor
public class MandatoryRequest {

  /**
   * Currency of the request
   */
  @HeaderParam
  @DefaultValue("IDR")
  private String currency;

  /**
   * IP address of requester
   */
  @HeaderParam
  @NotBlank
  @DefaultValue("0.0.0.0")
  private String clientIpAddress;

  /**
   * Unique session identifier of requester
   */
  @HeaderParam
  @NotBlank
  @DefaultValue("sessionId")
  private String clientSessionId;

  /**
   * Unique identifier of the request
   */
  @HeaderParam
  @NotBlank
  @DefaultValue("requestId")
  private String requestId;

  /**
   * Service identifier of where the request come from (this should be the artifactId of each service)
   */
  @HeaderParam
  @NotBlank
  @DefaultValue("gateway")
  private String serviceId;

  /**
   * Username of requestor
   */
  @HeaderParam
  @NotBlank
  @DefaultValue("guest")
  private String username;

  @Builder
  public MandatoryRequest(String currency,
      @NotBlank String clientIpAddress,
      @NotBlank String clientSessionId,
      @NotBlank String requestId, @NotBlank String serviceId,
      @NotBlank String username) {
    this.currency = currency;
    this.clientIpAddress = clientIpAddress;
    this.clientSessionId = clientSessionId;
    this.requestId = requestId;
    this.serviceId = serviceId;
    this.username = username;
  }
}
