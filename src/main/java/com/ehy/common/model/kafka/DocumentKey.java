package com.ehy.common.model.kafka;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DocumentKey {

  @JsonProperty("_id")
  private Map<String, String> _id;
}
