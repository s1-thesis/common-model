package com.ehy.common.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PageInfo {

  private Integer dataCount;
  private Integer pageIndex;
  private Integer pageCount;
  private Long totalData;

  @Builder
  public PageInfo(Integer dataCount, Integer pageIndex, Integer pageCount, Long totalData) {
    this.dataCount = dataCount;
    this.pageIndex = pageIndex;
    this.pageCount = pageCount;
    this.totalData = totalData;
  }
}
