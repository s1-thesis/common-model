package com.ehy.common.helper;

import com.ehy.common.model.BaseResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Instant;
import java.util.List;

public class BaseResponseHelper {

  private BaseResponseHelper() {
  }

  public static <T> BaseResponse<T> build(String code, String message, T data,
      List<String> errors) {
    return BaseResponse.<T>parentBuilder()
        .code(code)
        .message(message)
        .data(data)
        .errors(errors)
        .unixTimestamp(Instant.now())
        .build();
  }

  public static <T> String buildAsString(String code, String message, T data, List<String> errors)
      throws JsonProcessingException {
    return new ObjectMapper().writeValueAsString(build(code, message, data, errors));
  }
}
