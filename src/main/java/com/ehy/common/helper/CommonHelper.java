package com.ehy.common.helper;

import com.ehy.common.constant.ResponseCode;
import com.ehy.common.exception.BusinessLogicException;
import com.ehy.common.model.BaseResponse;
import com.ehy.common.model.MandatoryRequest;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;

public class CommonHelper {

  private CommonHelper() {
  }

  public static String generateUUID(){
    return UUID.randomUUID().toString();
  }

  public static Map<String, String> buildMandatoryRequestMap(MandatoryRequest mandatoryRequest) {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(Include.NON_NULL);
    return mapper.convertValue(mandatoryRequest, new TypeReference<Map<String, String>>() {
    });
  }

  public static String buildBasicAuthHeader(String username, String password) {
    return new String(Base64.getEncoder().encode((username + ":" + password).getBytes()));
  }

  public static <T> T handleRestError(BaseResponse<T> response) {
    if (!ResponseCode.SUCCESS.getCode().equals(response.getCode())) {
      throw new BusinessLogicException(
          response.getCode(), response.getMessage(), response.getErrors());
    }

    return response.getData();
  }
}
