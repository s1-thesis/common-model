package com.ehy.common.exception;

import com.ehy.common.constant.ResponseCode;
import java.io.Serializable;
import java.util.List;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BusinessLogicException extends RuntimeException implements Serializable {

  private static final long serialVersionUID = 8666296725349358904L;

  private String code;
  private String message;
  private List<String> errors;

  public BusinessLogicException(ResponseCode responseCode, List<String> errors) {
    this.code = responseCode.getCode();
    this.message = responseCode.getMessage();
    this.errors = errors;
  }

  public BusinessLogicException(String code, String message, List<String> errors) {
    this.code = code;
    this.message = message;
    this.errors = errors;
  }
}
