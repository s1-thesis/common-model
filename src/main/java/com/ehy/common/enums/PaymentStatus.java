package com.ehy.common.enums;

public enum PaymentStatus {
  PENDING, CONFIRMED, DENIED, CANCELLED, REFUNDED, REFUND_DENIED
}
