package com.ehy.common.enums;

public enum OperationType {

  INSERT, UPDATE, REPLACE, DELETE
}
