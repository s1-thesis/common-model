package com.ehy.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PaymentMethodType {

  BANK_TRANSFER("bank_transfer"), CREDIT_CARD("credit_card"), E_WALLET("gopay");

  String value;
}
