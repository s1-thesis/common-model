package com.ehy.common.enums;

public enum OrderStatus {
  ACTIVE, EXPIRED, PROCESS, CONFIRMED, FAILED, CANCELLED, REFUNDED
}
